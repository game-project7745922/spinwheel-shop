using UnityEngine;
using UnityEngine.UI;

namespace DickyArya.Card
{
    public class CardCharacter : MonoBehaviour
    {
        [SerializeField] private Image _characterImage;
        [SerializeField] private Image _cardBox;
        [SerializeField] private Color[] _cardColors;

        private Card _card;
        private Sprite _characterSprite;
        private CardRarity _cardRarity;

        public Card Card => _card;
        public CardRarity CardRarity => _cardRarity;

        public void Initiate(Card card)
        {
            _card = card;
            _characterSprite = card.CharacterSprite;
            _cardRarity = card.Rarity;
        }

        private void Start()
        {
            _characterImage.sprite = _characterSprite;
            CardUtility.SetCardBoxColor(_cardBox, _cardRarity,_cardColors);

        }


        public void Test()
        {
            Debug.Log("test");
        }

    }
}
