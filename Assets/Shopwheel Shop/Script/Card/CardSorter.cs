using System;
using System.Collections.Generic;

namespace DickyArya.Card
{
    public class CardSorter : IComparer<Card>
    {
        public int Compare(Card x, Card y)
        {
            // Mengurutkan berdasarkan rarity: common -> rare -> epic -> legendary

            CardRarity[] cardRarities = { CardRarity.Common, CardRarity.Rare, CardRarity.Epic, CardRarity.Legendary };

            int indexX = Array.IndexOf(cardRarities, x.Rarity);
            int indexY = Array.IndexOf(cardRarities, y.Rarity);

            return indexX.CompareTo(indexY); ;

        }
    }
}
