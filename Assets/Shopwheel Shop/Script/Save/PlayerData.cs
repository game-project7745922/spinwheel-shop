using System.Collections.Generic;
using System.Linq;
using DickyArya.Game;

namespace DickyArya.Save
{
    [System.Serializable]
    public class PlayerData
    {
        public int CoinsAmount;
        public int DiamondsAmount;

        public int SelectedCardIndex;

        public bool[] CardObtainedData;

        public PlayerData(GameManager gameManager)
        {
            CoinsAmount = gameManager.CoinsAmount;
            DiamondsAmount = gameManager.DiamondsAmount;

            var cards = gameManager.Cards;

            int index = -1;
            int selectedCardIndex = -1;

            foreach (var card in cards)
            {
                index++;
                if (card != gameManager.SelectedCard) continue;

                selectedCardIndex = index;
            }

            SelectedCardIndex = selectedCardIndex;

            List<bool> cardObtainedListTemp = new List<bool>();

            foreach (var card in cards)
                cardObtainedListTemp.Add(card.Obtained);

            CardObtainedData = cardObtainedListTemp.ToArray();
        }

        public PlayerData(GameManager gameManager, int coinsAmount, int diamondAmount, int selectedCardIndex = -1)
        {
            CoinsAmount = coinsAmount;
            DiamondsAmount = diamondAmount;
            SelectedCardIndex = selectedCardIndex;

            var cards = gameManager.Cards;

            List<bool> cardObtainedListTemp = new List<bool>();

            foreach (var card in cards)
                cardObtainedListTemp.Add(card.Obtained);

            CardObtainedData = cardObtainedListTemp.ToArray();

            if (CardObtainedData.Length != 0 || CardObtainedData != null)
                for (int i = 0; i < CardObtainedData.Length; i++)
                    CardObtainedData[i] = false;

        }

    }
}
