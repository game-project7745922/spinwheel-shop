using UnityEngine;

namespace DickyArya.Game
{
    using Card;
    using UI.Animation;
    using SceneHandler;
    using UnityEngine.UI;
    using EasyUI.PickerWheelUI;
    using TMPro;
    using DickyArya.Save;

    public class GameManager : MonoBehaviour
    {
        [SerializeField] private bool _shouldResetData;
        [SerializeField] private CardData _cardData;

        [SerializeField] private GameObject _shopPanel, _characterPanel;

        [SerializeField] private PickerWheel _pickerWheel;
        [SerializeField] private Button _wheelButton;

        private PlayerData _playerData;

        public CardRarityException CardRarityException;

        private const string LUCKY_SPIN_WHEELS_SCENE = "LuckyWheelScene";
        private const string SHOP_SCENE = "ShopScene";

        public int CoinsAmount { get; set; }
        public int DiamondsAmount { get; set; }

        public bool ToggleCharacterPanel { get; set; }
        public bool OnShopSceneInitiateOnce { get; set; }
        public bool OnLuckyWheelSceneInitiateOnce { get; set; }

        public Card SelectedCard { get; set; }
        [field: SerializeField] public Card[] Cards { get; private set; }

        public static GameManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Cards = _cardData.Cards.ToArray();

                if (_shouldResetData)
                    ResetData();

                _playerData = Load();


                CoinsAmount = _playerData.CoinsAmount;
                DiamondsAmount = _playerData.DiamondsAmount;

                Instance = this;

                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(gameObject);

        }

        private void Start()
        {

            if (_playerData != null)
            {
                for (int i = 0; i < Cards.Length; i++)
                {
                    if (_playerData.CardObtainedData.Length == Cards.Length)
                        Cards[i].Obtained = _playerData.CardObtainedData[i];

                    if (_playerData.SelectedCardIndex == -1) continue;

                    if (Cards[i] == Cards[_playerData.SelectedCardIndex])
                        SelectedCard = Cards[_playerData.SelectedCardIndex];
                }

            }

            Save();

            if (CoinsAmount > 9999)
                CoinsAmount = 9999;



            ToggleCharacterPanel = false;
            OnShopSceneInitiateOnce = true;
            OnLuckyWheelSceneInitiateOnce = false;

        }

        private void Update()
        {
            var isOnShopScene = SceneHandler.CurrentSceneName() == "ShopScene";

            if (isOnShopScene)
            {
                if (OnShopSceneInitiateOnce)
                {
                    ToggleCharacterPanel = false;
                    SetMainPanel();
                    MainPanelHandler();
                    OnShopSceneInitiateOnce = false;
                }
            }
            else
            {
                if (OnLuckyWheelSceneInitiateOnce)
                {
                    SetPickerWheel();
                    SetWheelBtn();
                    OnLuckyWheelSceneInitiateOnce = false;
                }
            }
        }

        #region Set Required GameObject/Function for ShopScene 
        public void SetMainPanel()
        {
            _shopPanel = GameObject.FindGameObjectWithTag("ShopPanel");
            _characterPanel = GameObject.FindGameObjectWithTag("CharacterPanel");
        }

        public void MainPanelHandler()
        {
            _shopPanel.SetActive(!ToggleCharacterPanel);
            _characterPanel.SetActive(ToggleCharacterPanel);
        }
        #endregion

        #region  Set Required Gameobject/Function for LuckyWheelScene

        public void SetPickerWheel()
        {
            PickerWheel pickerWheel = FindObjectOfType<PickerWheel>();

            switch (CardRarityException)
            {
                case CardRarityException.None:
                    pickerWheel.SetCards(Cards.GetAllCard());
                    break;
                case CardRarityException.Common:
                    pickerWheel.SetCards(Cards.GetAllCardsAboveCommon());
                    break;
                case CardRarityException.Rare:
                    pickerWheel.SetCards(Cards.GetAllCardsAboveRare());
                    break;
                case CardRarityException.Epic:
                    pickerWheel.SetCards(Cards.GetAllCardsAboveEpic());
                    break;

                default:
                    pickerWheel.SetCards(Cards.GetAllCard());
                    break;
            }

            _pickerWheel = pickerWheel;
        }

        public void SetWheelBtn()
        {
            _wheelButton = GameObject.FindGameObjectWithTag("WheelBtn").GetComponent<Button>();

            var btnText = _wheelButton.GetComponentInChildren<TextMeshProUGUI>();

            UIAnimation[] uiAnimation = FindObjectsOfType<UIAnimation>();

            UITogglePanelOnYAxisAnim uiGetItem = null;

            foreach (var ui in uiAnimation)
            {
                if (ui.gameObject.tag != "GetWheelItemPanel") continue;
                uiGetItem = (UITogglePanelOnYAxisAnim)ui;
            }

            _wheelButton.onClick.AddListener(() => OnWheelBtnClicked(_pickerWheel, btnText, uiGetItem));
        }

        public void OnWheelBtnClicked(PickerWheel pickerWheel, TextMeshProUGUI btnText, UITogglePanelOnYAxisAnim uiGetItem)
        {
            _wheelButton.interactable = false;
            btnText.text = "Spinning....";

            pickerWheel.Spin();

            pickerWheel.OnSpinEnd((cardWheel) =>
            {
                btnText.text = "Spin";

                int index = 0;

                foreach (var card in Cards)
                {
                    index++;

                    if (card != cardWheel) continue;

                    if (card.Obtained)
                    {
                        switch (card.Rarity)
                        {
                            case CardRarity.Common:
                                GainCoins(100);
                                break;

                            case CardRarity.Rare:
                                GainCoins(300);
                                break;

                            case CardRarity.Epic:
                                GainCoins(500);
                                break;

                            case CardRarity.Legendary:
                                GainCoins(1000);
                                break;
                        }
                    }
                    else
                    {
                        card.Obtained = true;
                    }
                }
                var uiText = uiGetItem.GetComponentInChildren<TextMeshProUGUI>();
                uiText.text = $"Get {cardWheel.CharacterSprite.name}\nRarity {cardWheel.Rarity}";
                uiGetItem.ToggleActive = true;
                uiGetItem.DOMoveY();
            });
        }

        #endregion

        #region CardException Function

        public void SetCardAllCards()
        {
            CardRarityException = CardRarityException.None;
        }

        public void SetCardCommonRarityException()
        {
            CardRarityException = CardRarityException.Common;
        }

        public void SetCardRareRarityException()
        {
            CardRarityException = CardRarityException.Rare;
        }

        public void SetCardEpicRarityException()
        {
            CardRarityException = CardRarityException.Epic;
        }

        #endregion

        #region Payment function region

        private void OnPaymentAmountNotEnough()
        {
            var uiPanelAnimations = GameObject.FindObjectsOfType<UIAnimation>();

            UITogglePanelOnYAxisAnim uiInsufficientPanel = null;

            foreach (var ui in uiPanelAnimations)
            {
                if (ui.gameObject.tag != "InsufficientPanel") continue;

                uiInsufficientPanel = (UITogglePanelOnYAxisAnim)ui;
            }

            uiInsufficientPanel.ToggleActive = true;
            uiInsufficientPanel.DOMoveY();
        }

        public void GainCoins(int coins)
        {
            var coinsAmount = CoinsAmount;
            coinsAmount += coins;

            if (coinsAmount > 9999)
                CoinsAmount = 9999;
            else
                CoinsAmount = coinsAmount;

            Save();
        }

        public void OnBuyWithCoins(int coins)
        {
            var coinsAmount = CoinsAmount;
            coinsAmount -= coins;

            OnShopSceneInitiateOnce = false;
            OnLuckyWheelSceneInitiateOnce = true;

            if (coinsAmount < 0) OnPaymentAmountNotEnough();
            else SceneHandler.LoadScene(LUCKY_SPIN_WHEELS_SCENE, () =>
            {
                CoinsAmount = coinsAmount;
                Save();
            });

        }

        public void OnBuyWithDiamonds(int diamonds)
        {
            var diamondAmount = DiamondsAmount;
            diamondAmount -= diamonds;

            OnShopSceneInitiateOnce = false;
            OnLuckyWheelSceneInitiateOnce = true;

            if (diamondAmount < 0) OnPaymentAmountNotEnough();
            else SceneHandler.LoadScene(LUCKY_SPIN_WHEELS_SCENE, () =>
            {
                DiamondsAmount = diamondAmount;
                Save();
            });

        }

        #endregion

        #region Save/Load function
        public void Save()
        {
            SaveSystem.SaveData(this);
        }

        public PlayerData Load()
        {
            return SaveSystem.LoadData();
        }

        public void ResetData()
        {
            SaveSystem.ResetData(this);
        }

        #endregion
    }
}
