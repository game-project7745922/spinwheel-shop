﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using System.Collections.Generic;
using DickyArya.Card;
using TMPro;
using DickyArya.Game;

namespace EasyUI.PickerWheelUI
{

   public class PickerWheel : MonoBehaviour
   {

      [Header("References :")]
      [SerializeField] private GameObject linePrefab;
      [SerializeField] private Transform linesParent;

      [Space]
      [SerializeField] private Transform PickerWheelTransform;
      [SerializeField] private Transform wheelCircle;
      [SerializeField] private GameObject wheelPiecePrefab;
      [SerializeField] private Transform wheelPiecesParent;

      // [Space]
      // [Header ("Sounds :")]
      // [SerializeField] private AudioSource audioSource ;
      // [SerializeField] private AudioClip tickAudioClip ;
      // [SerializeField] [Range (0f, 1f)] private float volume = .5f ;
      // [SerializeField] [Range (-3f, 3f)] private float pitch = 1f ;

      [Space]
      [Header("Picker wheel settings :")]
      [Range(1, 20)] public int spinDuration = 8;
      [SerializeField][Range(.2f, 2f)] private float wheelSize = 1f;

      [Space]
      [Header("Picker wheel pieces :")]
      public CardData cardData;
      public Card[] cards;

      // Events
      private UnityAction onSpinStartEvent;
      private UnityAction<Card> onSpinEndEvent;


      private bool _isSpinning = false;

      public bool IsSpinning { get { return _isSpinning; } }


      private Vector2 pieceMinSize = new Vector2(81f, 146f);
      private Vector2 pieceMaxSize = new Vector2(144f, 213f);
      private int piecesMin = 2;
      private int piecesMax = 20;

      private float pieceAngle;
      private float halfPieceAngle;
      private float halfPieceAngleWithPaddings;


      private double accumulatedWeight;
      private System.Random rand = new System.Random();

      private List<int> nonZeroChancesIndices = new List<int>();

      public UnityEvent<Card[]> OnFilterCards;

      private void Start()
      {
         switch (GameManager.Instance.CardRarityException)
         {
            case CardRarityException.None:
               cards = GameManager.Instance.Cards.GetAllCard();
               break;

            case CardRarityException.Common:
               cards = GameManager.Instance.Cards.GetAllCardsAboveCommon();
               break;

            case CardRarityException.Rare:
               cards = GameManager.Instance.Cards.GetAllCardsAboveRare();
               break;

            case CardRarityException.Epic:
               cards = GameManager.Instance.Cards.GetAllCardsAboveEpic();
               break;
            
            default:
               cards = GameManager.Instance.Cards.GetAllCard();
               break;
         }

         piecesMax = cards.Length;

         if (cards.Length > piecesMax || cards.Length < piecesMin)
         {
            Debug.LogError("[ PickerWheelwheel ]  pieces length must be between " + piecesMin + " and " + piecesMax);
            return;
         }

         pieceAngle = 360 / cards.Length;
         halfPieceAngle = pieceAngle / 2f;
         halfPieceAngleWithPaddings = halfPieceAngle - (halfPieceAngle / 4f);

         Generate();

         CalculateWeightsAndIndices();
         if (nonZeroChancesIndices.Count == 0)
            Debug.LogError("You can't set all pieces chance to zero");


         // SetupAudio () ;

      }

      // private void SetupAudio () {
      //    audioSource.clip = tickAudioClip ;
      //    audioSource.volume = volume ;
      //    audioSource.pitch = pitch ;
      // }

      private void Generate()
      {
         wheelPiecePrefab = InstantiatePiece();

         RectTransform rt = wheelPiecePrefab.transform.GetChild(0).GetComponent<RectTransform>();
         float pieceWidth = Mathf.Lerp(pieceMinSize.x, pieceMaxSize.x, 1f - Mathf.InverseLerp(piecesMin, piecesMax, cards.Length));
         float pieceHeight = Mathf.Lerp(pieceMinSize.y, pieceMaxSize.y, 1f - Mathf.InverseLerp(piecesMin, piecesMax, cards.Length));
         rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, pieceWidth);
         rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, pieceHeight);

         for (int i = 0; i < cards.Length; i++)
            DrawPiece(i);

         Destroy(wheelPiecePrefab);
      }

      private void DrawPiece(int index)
      {
         Card card = cards[index];
         Transform pieceTrns = InstantiatePiece().transform.GetChild(0);

         pieceTrns.GetChild(0).GetComponent<Image>().sprite = card.CharacterSprite;

         //Line
         Transform lineTrns = Instantiate(linePrefab, linesParent.position, Quaternion.identity, linesParent).transform;
         lineTrns.RotateAround(wheelPiecesParent.position, Vector3.back, (pieceAngle * index) + halfPieceAngle);

         pieceTrns.RotateAround(wheelPiecesParent.position, Vector3.back, pieceAngle * index);
      }

      private GameObject InstantiatePiece()
      {
         return Instantiate(wheelPiecePrefab, wheelPiecesParent.position, Quaternion.identity, wheelPiecesParent);
      }


      public void Spin()
      {
         if (!_isSpinning)
         {
            _isSpinning = true;
            if (onSpinStartEvent != null)
               onSpinStartEvent.Invoke();

            int index = GetRandomPieceIndex();
            Card card = cards[index];

            float cardChance = 0;

            switch (card.Rarity)
            {
               case CardRarity.Common:
                  cardChance = 80f;
                  break;

               case CardRarity.Rare:
                  cardChance = 60f;
                  break;

               case CardRarity.Epic:
                  cardChance = 30f;
                  break;

               case CardRarity.Legendary:
                  cardChance = 5f;
                  break;

            }

            if (cardChance == 0 && nonZeroChancesIndices.Count != 0)
            {
               index = nonZeroChancesIndices[Random.Range(0, nonZeroChancesIndices.Count)];
               card = cards[index];
            }

            float angle = -(pieceAngle * index);

            float rightOffset = (angle - halfPieceAngleWithPaddings) % 360;
            float leftOffset = (angle + halfPieceAngleWithPaddings) % 360;

            float randomAngle = Random.Range(leftOffset, rightOffset);

            Vector3 targetRotation = Vector3.back * (randomAngle + 2 * 360 * spinDuration);

            //float prevAngle = wheelCircle.eulerAngles.z + halfPieceAngle ;
            float prevAngle, currentAngle;
            prevAngle = currentAngle = wheelCircle.eulerAngles.z;

            bool isIndicatorOnTheLine = false;

            wheelCircle
            .DORotate(targetRotation, spinDuration, RotateMode.Fast)
            .SetEase(Ease.InOutQuart)
            .OnUpdate(() =>
            {
               float diff = Mathf.Abs(prevAngle - currentAngle);
               if (diff >= halfPieceAngle)
               {
                  if (isIndicatorOnTheLine)
                  {
                     // audioSource.PlayOneShot (audioSource.clip) ;
                  }
                  prevAngle = currentAngle;
                  isIndicatorOnTheLine = !isIndicatorOnTheLine;
               }
               currentAngle = wheelCircle.eulerAngles.z;
            })
            .OnComplete(() =>
            {
               _isSpinning = false;
               if (onSpinEndEvent != null)
                  onSpinEndEvent.Invoke(card);

               onSpinStartEvent = null;
               onSpinEndEvent = null;
            });

         }
      }

      private void FixedUpdate()
      {

      }

      public void OnSpinStart(UnityAction action)
      {
         onSpinStartEvent = action;
      }

      public void OnSpinEnd(UnityAction<Card> action)
      {
         onSpinEndEvent = action;
      }


      private int GetRandomPieceIndex()
      {
         double r = rand.NextDouble() * accumulatedWeight;

         for (int i = 0; i < cards.Length; i++)
            if (cards[i].Weight >= r)
               return i;

         return 0;
      }

      private void CalculateWeightsAndIndices()
      {
         for (int i = 0; i < cards.Length; i++)
         {
            Card card = cards[i];

            float cardChance = 0;

            switch (card.Rarity)
            {
               case CardRarity.Common:
                  cardChance = 80f;
                  break;

               case CardRarity.Rare:
                  cardChance = 60f;
                  break;

               case CardRarity.Epic:
                  cardChance = 10f;
                  break;

               case CardRarity.Legendary:
                  cardChance = 5f;
                  break;

            }

            //add weights:
            accumulatedWeight += cardChance;
            card.Weight = accumulatedWeight;

            //add index :
            card.Index = i;

            //save non zero chance indices:
            if (cardChance > 0)
               nonZeroChancesIndices.Add(i);
         }
      }

      private void OnValidate()
      {
         if (PickerWheelTransform != null)
            PickerWheelTransform.localScale = new Vector3(wheelSize, wheelSize, 1f);

      }

      public void SetCards(Card[] cards)
      {
         this.cards = cards;
      }
   }
}