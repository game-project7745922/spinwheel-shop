using System.Collections.Generic;
using UnityEngine;

namespace DickyArya.Card
{
    [CreateAssetMenu(fileName = "CardData", menuName = "Spinwheel Shop/CardData", order = 0)]
    public class CardData : ScriptableObject
    {
        public List<Card> Cards = new List<Card>();
    }
}
