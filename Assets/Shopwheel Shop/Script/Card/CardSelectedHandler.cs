using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DickyArya.Card
{
    using DickyArya.Save;
    using Game;

    public class CardSelectedHandler : MonoBehaviour
    {
        [SerializeField] private Image _characterImage;
        [SerializeField] private TextMeshProUGUI _charaNotSelectedText;
        [SerializeField] private Image _cardBox;
        [SerializeField] private Color[] _cardColors;

        private bool _initiateOnceOnUpdate = true;

        private GameManager _gameManager;

        public void Initiate(Card card)
        {
            _characterImage.sprite = card.CharacterSprite;
            CardUtility.SetCardBoxColor(_cardBox, card.Rarity, _cardColors);
            _gameManager.Save();
        }

        private void Start()
        {
            _gameManager = GameManager.Instance;
        }

        private void Update()
        {
            if (_initiateOnceOnUpdate)
                if (_gameManager.SelectedCard != null)
                    Initiate(_gameManager.SelectedCard);
                

            _characterImage.gameObject.SetActive(_gameManager.SelectedCard != null);
            _charaNotSelectedText.gameObject.SetActive(_gameManager.SelectedCard == null);

        }
    }
}
