using UnityEngine;
using UnityEngine.UI;

namespace DickyArya.UI
{
    [RequireComponent(typeof(GridLayoutGroup))]
    public class UIGridContentHandler : MonoBehaviour
    {
        private RectTransform _rectTransform;
        private GridLayoutGroup _gridLayoutGroup;

        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _gridLayoutGroup = GetComponent<GridLayoutGroup>();

        }

        public void AdjustHeight()
        {
            int childCount = 0;

            foreach (Transform child in transform)
            {
                if (child.gameObject.activeInHierarchy)
                    childCount++;
            }

            int gridConstraintCount = _gridLayoutGroup.constraintCount;

            float yCellSize = _gridLayoutGroup.cellSize.y;
            float ySpacing = _gridLayoutGroup.spacing.y;
            float heightMultipler = (float)childCount / (float)gridConstraintCount;

            float decimalValue = Mathf.Abs(heightMultipler % 1.0f);

            bool isDecimal = decimalValue > 0.0f;

            float decimalAdder = 1f - decimalValue;

            float heightAddVal = yCellSize + ySpacing;

            float height = isDecimal ? heightAddVal * (heightMultipler + decimalAdder) : heightAddVal * heightMultipler;

            _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, height);

        }
    }
}
