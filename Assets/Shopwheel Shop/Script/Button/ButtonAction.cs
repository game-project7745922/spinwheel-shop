using UnityEngine;

namespace DickyArya.Button
{
    using Game;
    using SceneHandler;

    public class ButtonAction : MonoBehaviour
    {
        public void MoveToScene(string sceneName)
        {
            SceneHandler.LoadScene(sceneName);
        }

        public void SetCardsToAllCard()
        {
            GameManager.Instance.SetCardAllCards();
        }

        public void SetCardCommonRarityException()
        {
            GameManager.Instance.SetCardCommonRarityException();
        }

        public void SetCardRareRarityException()
        {
            GameManager.Instance.SetCardRareRarityException();
        }

        public void SetCardEpicRarityException()
        {
            GameManager.Instance.SetCardEpicRarityException();
        }

        public void BuyWithCoins(int value)
        {
            GameManager.Instance.OnBuyWithCoins(value);
        }

        public void BuyWithDiamonds(int value)
        {
            GameManager.Instance.OnBuyWithDiamonds(value);
        }

        public void MoveToShopScene()
        {
            GameManager.Instance.OnShopSceneInitiateOnce = true;
            GameManager.Instance.OnLuckyWheelSceneInitiateOnce = false;
            MoveToScene("ShopScene");
        }
    }
}
