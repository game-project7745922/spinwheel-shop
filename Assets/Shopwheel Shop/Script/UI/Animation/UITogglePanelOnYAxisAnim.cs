
using UnityEngine;
using UnityEngine.UI;

namespace DickyArya.UI.Animation
{
    public class UITogglePanelOnYAxisAnim : UIAnimation
    {
        [SerializeField] private Image _blackScreenPanel;
        [SerializeField] private float _endValue;
        [SerializeField] private float _duration;

        public bool ToggleActive { get; set; }

        private void Start()
        {
            ToggleActive = false;
        }

        public void DOMoveY()
        {
            DoMoveY(ToggleActive ? 0f : _endValue, _duration, () => _blackScreenPanel.enabled = ToggleActive);
        }
    }
}
