using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace DickyArya.Card
{
    public static class CardUtility
    {
        #region Card filter function


        public static Card[] GetAllCardsByStatus(this Card[] cards, CardStatus cardStatus)
        {
            var cardsFiltered = cards.Where(
                card => (cardStatus == CardStatus.Obtained) ? card.Obtained : !card.Obtained).ToArray();

            return cardsFiltered;
        }

        public static Card[] GetCardByRarity(this Card[] cards, CardRarity cardRarity, CardStatus cardStatus)
        {
            var allCards = cards.GetAllCardsByStatus(cardStatus);

            return allCards.Where(card => card.Rarity == cardRarity).ToArray();
        }

        public static Card[] GetAllCard(this Card[] cards)
        {
            return cards;
        }

        public static Card[] GetAllCardsAboveCommon(this Card[] cards)
        {
            return cards.Where(card => card.CardRarityException(CardRarity.Common)).ToArray();
        }

        public static Card[] GetAllCardsAboveRare(this Card[] cards)
        {
            return cards.Where(card => card.CardRarityException(CardRarity.Rare)).ToArray();
        }

        public static Card[] GetAllCardsAboveEpic(this Card[] cards)
        {
            return cards.Where(card => card.CardRarityException(CardRarity.Epic)).ToArray();
        }

        public static bool CardRarityException(this Card card, CardRarity cardRarityException)
        {
            bool isNotExceptionCard = false;
            switch (cardRarityException)
            {
                case CardRarity.Common:
                    isNotExceptionCard = card.Rarity != CardRarity.Common;
                    break;

                case CardRarity.Rare:
                    isNotExceptionCard = card.Rarity != CardRarity.Common
                                            && card.Rarity != CardRarity.Rare;
                    break;

                case CardRarity.Epic:
                    isNotExceptionCard = card.Rarity == CardRarity.Legendary;
                    break;
            }

            return isNotExceptionCard;

        }

        #endregion

        public static void SetCardBoxColor(Image cardBoxImg, CardRarity cardRarity, Color[] cardColors)
        {
            if (cardColors.Length != 4) return;

            int cardBoxIndex = 0;

            for (int i = 0; i < cardColors.Length; i++)
            {
                switch (cardRarity)
                {
                    case CardRarity.Common:
                        cardBoxIndex = 0;
                        break;

                    case CardRarity.Rare:
                        cardBoxIndex = 1;
                        break;

                    case CardRarity.Epic:
                        cardBoxIndex = 2;
                        break;

                    case CardRarity.Legendary:
                        cardBoxIndex = 3;
                        break;

                    default:
                        cardBoxIndex = 0;
                        break;
                }
            }

            cardBoxImg.color = cardColors[cardBoxIndex];

        }
    }
}
