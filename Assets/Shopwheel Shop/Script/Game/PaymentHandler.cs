using TMPro;
using UnityEngine;

namespace DickyArya.Game
{
    public class PaymentHandler : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _coinsText;
        [SerializeField] private TextMeshProUGUI _diamondsText;

        private void Update()
        {
            var gameManager = GameManager.Instance;
            _coinsText.text =  gameManager.CoinsAmount.ToString();
            _diamondsText.text =  gameManager.DiamondsAmount.ToString();
        }
    }
}
