using UnityEngine;

namespace DickyArya.Card
{
    [System.Serializable]
    public class Card
    {
        public Sprite CharacterSprite;
        public CardRarity Rarity;
        public bool Obtained;

        [HideInInspector]
        public double Weight;

        [HideInInspector]
        public int Index;
    }

    public enum CardRarity
    {
        Common,
        Rare,
        Epic,
        Legendary,
    }

    public enum CardStatus
    { 
        Obtained,
        Not_Obtained,
    }

    public enum CardRarityException 
    { 
        None,
        Common,
        Rare,
        Epic,
    }
}
