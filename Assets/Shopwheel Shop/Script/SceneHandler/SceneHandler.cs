using UnityEngine.SceneManagement;

namespace DickyArya.SceneHandler
{
    public static class SceneHandler
    {
        public static void LoadScene(string scene, System.Action onSceneToLoad = null, LoadSceneMode mode = LoadSceneMode.Single)
        {
            onSceneToLoad?.Invoke();
            SceneManager.LoadScene(scene, mode);
        }

        public static string CurrentSceneName()
        {
            return SceneManager.GetActiveScene().name;
        }
    }
}
