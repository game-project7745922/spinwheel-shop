using UnityEngine;
using UnityEngine.EventSystems;

namespace DickyArya.Button
{
    using Card;
    using Game;

    [RequireComponent(typeof(CardCharacter))]
    public class CardButton : MonoBehaviour, IEventSystemHandler, IPointerDownHandler
    {
        private EventSystem _eventSystem;
        private CardCharacter _cardCharacter;
        private bool _isSelected;

        private void Start()
        {
            _eventSystem = EventSystem.current;
            _cardCharacter = GetComponent<CardCharacter>();
        }

        private void Update()
        {
            _isSelected = _eventSystem.currentSelectedGameObject == gameObject;

        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (_isSelected) return;
            if (eventData.button != PointerEventData.InputButton.Left) return;

            _eventSystem.SetSelectedGameObject(gameObject, eventData);

            GameManager.Instance.SelectedCard = _cardCharacter.Card;

            var cardSelect = FindObjectOfType<CardSelectedHandler>();

            cardSelect.Initiate(GameManager.Instance.SelectedCard);


        }
    }
}
