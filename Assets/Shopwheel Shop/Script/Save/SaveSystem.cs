
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace DickyArya.Save
{
    using Game;

    public static class SaveSystem
    {
        public static void SaveData(GameManager gameManager)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/playerData.playerdata";
            FileStream stream = new FileStream(path, FileMode.Create);

            PlayerData playerData = new PlayerData(gameManager);

            formatter.Serialize(stream, playerData);

            stream.Close();
        }

        public static PlayerData LoadData()
        {
            string path = Application.persistentDataPath + "/playerData.playerdata";

            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                PlayerData playerData = (PlayerData)formatter.Deserialize(stream);

                stream.Close();

                return playerData;
            }
            else
                return null;
        }

        public static void ResetData(GameManager gameManager)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/playerData.playerdata";
            FileStream stream = new FileStream(path, FileMode.Create);

            PlayerData playerData = new PlayerData(gameManager, 200, 60);

            formatter.Serialize(stream, playerData);

            stream.Close();
        }
    }
}
