using UnityEngine;
using UnityEngine.UI;

namespace DickyArya.UI
{
    public class UIVerticalContentHandler : MonoBehaviour
    {
        private RectTransform _rectTransform;
        private VerticalLayoutGroup _verticalLayout;

        float _height;

        bool _initiatedOnceOnUpdate = false;

        private void Start()
        {
            _rectTransform = GetComponent<RectTransform>();
            _verticalLayout = GetComponent<VerticalLayoutGroup>();

            _initiatedOnceOnUpdate = true;

        }

        private void Update()
        {
            if (!_initiatedOnceOnUpdate) return;

            AdjustHeight();
            _initiatedOnceOnUpdate = false;
        }

        public void AdjustHeight()
        {
            _height = 0;

            for (int i = 0; i < transform.childCount; i++)
            {
                int index = i;
                RectTransform childRT = transform.GetChild(index).GetComponent<RectTransform>();
                _height += childRT.sizeDelta.y + _verticalLayout.spacing;
            }

            _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, _height);
        }
    }
}

