using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace DickyArya.Card
{
    using Game;

    public class CardHandler : MonoBehaviour
    {
        [SerializeField] private CardCharacter _cardCharacterPrefab;
        [SerializeField] private CardStatus _cardStatus;
        [SerializeField] private UnityEvent _onAllCardInstantiated;

        private Card[] _cards;
        private List<CardCharacter> _cardCharacters = new List<CardCharacter>();

        private void Start()
        {
            _cards = GameManager.Instance.Cards.GetAllCardsByStatus(_cardStatus);

            CardSorter cardSorter = new CardSorter();

            Array.Sort(_cards, cardSorter);

            InstantiateCards();

            if (transform.childCount == _cards.Length)
                _onAllCardInstantiated?.Invoke();
        }

        #region Get Cards Function

        public void GetAllCards()
        {
            _cards = GameManager.Instance.Cards.GetAllCardsByStatus(_cardStatus);
            SetAllCards();
        }

        public void GetCommonCards()
        {
            _cards = GameManager.Instance.Cards.GetCardByRarity(CardRarity.Common, _cardStatus);
            SetCards(CardRarity.Common);
        }

        public void GetRareCards()
        {
            _cards = GameManager.Instance.Cards.GetCardByRarity(CardRarity.Rare, _cardStatus);
            SetCards(CardRarity.Rare);
        }

        public void GetEpicCards()
        {
            _cards = GameManager.Instance.Cards.GetCardByRarity(CardRarity.Epic, _cardStatus);
            SetCards(CardRarity.Epic);
        }

        public void GetLegendaryCards()
        {
            _cards = GameManager.Instance.Cards.GetCardByRarity(CardRarity.Legendary, _cardStatus);
            SetCards(CardRarity.Legendary);
        }
        #endregion

        #region Set Cards UI Function

        private void InstantiateCards()
        {
            foreach (var card in _cards)
            {
                var cardGO = Instantiate(_cardCharacterPrefab, transform);
                cardGO.Initiate(card);

                _cardCharacters.Add(cardGO);
            }
        }

        private void SetAllCards()
        {
            foreach (var cardCharacter in _cardCharacters)
                cardCharacter.gameObject.SetActive(false);

            foreach (var cardCharacter in _cardCharacters)
                cardCharacter.gameObject.SetActive(true);

        }

        private void SetCards(CardRarity cardRarity)
        {
            foreach (var cardCharacter in _cardCharacters)
                cardCharacter.gameObject.SetActive(false);

            CardCharacter[] cardCharacters = _cardCharacters.Where(cardCharacter => cardCharacter.CardRarity == cardRarity).ToArray();

            foreach (var cardCharacter in cardCharacters)
                cardCharacter.gameObject.SetActive(true);

        }
        #endregion

    }
}
