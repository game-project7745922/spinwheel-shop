using DG.Tweening;
using UnityEngine;

namespace DickyArya.UI.Animation
{
    public class UIAnimation : MonoBehaviour
    {
        protected void DoMoveY(float endValue, float duration = 1f, System.Action OnAnimComplete = null, bool snapping = false)
        {
            transform.DOMoveY(endValue, duration, false).OnComplete(() => OnAnimComplete?.Invoke());
        }
    }
}
